package com.example.student.pojo.vo;

import com.example.student.pojo.ClassDO;
import com.example.student.pojo.StudentDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;


@Data
@ToString(callSuper = true)
public class ClassVO extends ClassDO {

    private List<StudentDO> students;

}
