package com.example.student.pojo.vo;

import com.example.student.pojo.StudentDO;
import lombok.Data;
import lombok.ToString;


@Data
@ToString(callSuper = true)
public class StudentVO extends StudentDO {

    private String className;

}
