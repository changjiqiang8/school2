package com.example.student.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 课程表
 * @author closer
 */
@Data
@TableName("course")
public class CourseDO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String courseNumber;

    private String courseName;

    private String description;

    private Integer isDel;

}
