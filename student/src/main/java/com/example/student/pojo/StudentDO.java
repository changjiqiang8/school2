package com.example.student.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("student")
public class StudentDO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private String classNumber;

    private Integer age;

    private Integer sex;

    private Integer isDel;


}
