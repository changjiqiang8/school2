package com.example.student.pojo.vo;

import com.example.student.pojo.CourseDO;
import com.example.student.pojo.StudentDO;
import lombok.Data;

import java.util.List;

@Data
public class CourseVO extends StudentDO {

    private List<CourseDO> courseList;

}
