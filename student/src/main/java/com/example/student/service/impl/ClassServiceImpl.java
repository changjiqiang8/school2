package com.example.student.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student.mapper.ClassMapper;
import com.example.student.pojo.ClassDO;
import com.example.student.pojo.vo.ClassVO;
import com.example.student.service.ClassService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClassServiceImpl extends ServiceImpl<ClassMapper, ClassDO> implements ClassService {
    @Override
    @Transactional
    public int add(ClassDO classDO) {
        int rows = this.baseMapper.insert(classDO);
        return rows;
    }

    @Override
    @Transactional
    public int deleteById(Long id) {
        int rows = deleteById(id);
        return rows;
    }

    @Override
    public ClassVO getClassAndStudents(String id) {
        ClassVO classVOList = this.baseMapper.getClassAndStudents(id);
        return classVOList;
    }

}
