package com.example.student.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student.pojo.ClassDO;
import com.example.student.pojo.vo.ClassVO;

import java.util.List;


public interface ClassService extends IService<ClassDO> {
    int add(ClassDO classDO);

    int deleteById(Long id);

    ClassVO  getClassAndStudents(String number);
}
