package com.example.student.controller;


import com.example.student.common.Result;
import com.example.student.pojo.StudentDO;
import com.example.student.pojo.vo.CourseVO;
import com.example.student.pojo.vo.StudentVO;
import com.example.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/student")
public class StudentController {


    @Autowired
    private StudentService studentService;


    /**
     * 添加学生
     * @param studentDO
     * @return
     */
    @PostMapping
    public Result add(@RequestBody StudentDO studentDO){
        if (ObjectUtils.isEmpty(studentDO)){
            return Result.result("204","新增参数错误！");
        }
        int rows = studentService.add(studentDO);
        return rows>0?Result.result("200","新增学生成功！"):Result.result("204","新增学生失败！");
    }



    /**
     * 获取所有学生
     * @return
     */
    @GetMapping
    public Result listAllStudent(){
        List<StudentDO> list = studentService.list();
        return Result.success(list);
    }


    /**
     * 根据id修改学生
     * @param studentDO
     * @return
     */
    @PutMapping
    public Result updateById(@RequestBody StudentDO studentDO){
        if (ObjectUtils.isEmpty(studentDO)){
            return Result.result("204","修改参数错误！");
        }
        boolean update = studentService.updateById(studentDO);
        return update?Result.result("200","修改学生成功！"):Result.result("204","修改学生失败！");
    }



    /**
     * 根据id删除学生
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteStudentById(@PathVariable(value = "id")Long id){
        if (ObjectUtils.isEmpty(id)){
            return Result.result("204","缺少学生id参数！");
        }
        int rows = studentService.deleteById(id);
        return rows > 0?Result.result("200","删除成功"):Result.result("204","删除学生失败！");
    }


    /**
     * 根据班级编号获取该班级所有学生
     * @param classNumber
     * @return
     */
    @GetMapping(params = "classNumber")
    public Result getStudentsByClassNumber2(@RequestParam("classNumber")String classNumber){
        List<StudentVO> studentList =  studentService.getStudentsByClassNumber(classNumber);
        return Result.result("200","查找成功！",studentList);
    }


    @GetMapping(params = {"studentId"})
    public Result getStudentAndCourse(@RequestParam("studentId")Long studentId){
        CourseVO courseVO = studentService.getStudentAndCourse(studentId);
        return Result.result("200","查询成功！",courseVO);
    }



}
