package com.example.student.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.student.pojo.StudentDO;
import com.example.student.pojo.vo.StudentVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author closer
 */
@Mapper
public interface StudentMapper extends BaseMapper<StudentDO> {


    /**
     * 根据班级编号获取学生
     * @param classNumber
     * @return
     */
    List<StudentVO> getStudentsByClassNumber(@Param("classNumber") String classNumber);
}
