package com.example.student.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.student.pojo.ClassDO;
import com.example.student.pojo.vo.ClassVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ClassMapper extends BaseMapper<ClassDO> {

    ClassVO getClassAndStudents(String id);
}
