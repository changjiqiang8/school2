package com.example.student.config;


import com.example.feignservice.course.fallback.CourseClientFallbackFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SentinelConfiguration {

    @Bean
    public CourseClientFallbackFactory courseClientFallbackFactory(){
        return new CourseClientFallbackFactory();
    }


}
