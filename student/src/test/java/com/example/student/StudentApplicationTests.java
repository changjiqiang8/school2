package com.example.student;

import com.example.student.mapper.ClassMapper;
import com.example.student.pojo.vo.ClassVO;
import com.example.student.service.ClassService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = {StudentApplication.class})
class StudentApplicationTests {


    @Autowired
    private ClassService classService;

    /**
     * 测试获取班级信息及其学生
     */
    @Test
    void testGetStudentsByClassNumber() {

        ClassVO classVO = classService.getClassAndStudents("A001");

        System.out.println(classVO);

    }

}
