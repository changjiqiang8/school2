package com.example.course.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.course.mapper.ChooseCourseMapper;
import com.example.course.pojo.ChooseCourseDO;
import com.example.course.service.ChooseCourseService;
import org.springframework.stereotype.Service;


/**
 * @author closer
 */
@Service
public class ChooseCourseServiceImpl extends ServiceImpl<ChooseCourseMapper, ChooseCourseDO> implements ChooseCourseService {
}
