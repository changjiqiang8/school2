package com.example.course.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.course.mapper.CourseMapper;
import com.example.course.pojo.CourseDO;
import com.example.course.service.CourseService;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author closer
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, CourseDO> implements CourseService {

    @Override
    public List<CourseDO> getCourseByStudentId(Long studentId) {
        List<CourseDO> courseDOList = this.baseMapper.getCourseByStudentId(studentId);
        return courseDOList;
    }
}
