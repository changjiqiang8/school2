package com.example.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.course.pojo.CourseDO;

import java.util.List;

public interface CourseService extends IService<CourseDO> {
    List<CourseDO> getCourseByStudentId(Long studentId);
}
