package com.example.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.course.pojo.ChooseCourseDO;

/**
 * @author closer
 */
public interface ChooseCourseService extends IService<ChooseCourseDO> {
}
