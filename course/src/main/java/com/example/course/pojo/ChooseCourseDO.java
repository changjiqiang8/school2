package com.example.course.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 选课表
 * @author closer
 */
@Data
@TableName("choose_course")
public class ChooseCourseDO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String courseNumber;

    private Long stuId;


}
