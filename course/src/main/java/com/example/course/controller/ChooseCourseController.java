package com.example.course.controller;


import com.example.course.common.Result;
import com.example.course.pojo.ChooseCourseDO;
import com.example.course.service.ChooseCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author closer
 */
@RestController
@RequestMapping("/api/v1/choose-course")
public class ChooseCourseController {


    @Autowired
    private ChooseCourseService chooseCourseService;


    /**
     * 学生选课
     * @param chooseCourseDO
     * @return
     */
    @PostMapping
    public Result addChooseCourse(@RequestBody ChooseCourseDO chooseCourseDO){
        boolean save = chooseCourseService.save(chooseCourseDO);
        return save?Result.result("200","新增成功！"):Result.result("204","新增失败！");
    }

}
