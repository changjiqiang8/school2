package com.example.course.controller;


import com.example.course.common.Result;
import com.example.course.pojo.CourseDO;
import com.example.course.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author closer
 */
@RestController
@RequestMapping("/api/v1/course")
public class CourseController {


    @Autowired
    private CourseService courseService;

    /**
     * 获取所有课程
     * @return
     */
    @GetMapping
    public Result getAllCourse(){

        List<CourseDO> list = courseService.list();

        return Result.result("200","获取所有课程成功！",list);
    }


    /**
     * 新增课程
     * @param courseDO
     * @return
     */
    @PostMapping
    public Result add(@RequestBody CourseDO courseDO){
        boolean save = courseService.save(courseDO);
        return save?Result.result("200","新增课程成功！"):Result.result("204","新增课程失败！");
    }


    /**
     * 获取学生的所选课程信息
     * @return
     */
    @GetMapping(params = {"studentId"})
    public Result getCourseByStudentId(@RequestParam("studentId") Long studentId){
//        Integer a = 10/0;
        List<CourseDO> list = courseService.getCourseByStudentId(studentId);
        return Result.result("200","获取学生所选课程成功！",list);
    }


}
