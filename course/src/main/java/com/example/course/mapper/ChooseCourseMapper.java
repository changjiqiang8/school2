package com.example.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.course.pojo.ChooseCourseDO;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author closer
 */
@Mapper
public interface ChooseCourseMapper extends BaseMapper<ChooseCourseDO> {
}
