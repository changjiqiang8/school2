package com.example.course.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.course.pojo.CourseDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author closer
 */
@Mapper
public interface CourseMapper extends BaseMapper<CourseDO> {


    /**
     * 根据学生id获取学生所选课程
     * @param studentId
     * @return
     */
    List<CourseDO> getCourseByStudentId(@Param("studentId") Long studentId);
}
