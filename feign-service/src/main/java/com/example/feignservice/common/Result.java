package com.example.feignservice.common;


import com.example.feignservice.enums.ResultCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T>{

    private String code;

    private String msg;

    //返回数据
    private  T data;


    public static <T> Result success(){
        Result<T> result=new Result<>();
        result.setCode(ResultCodeEnum.SUCCESS.code);
        result.setMsg(ResultCodeEnum.SUCCESS.msg);
        return result;
    }


    public static <T> Result success(T data){
        Result<T> result=new Result<>();
        result.setCode(ResultCodeEnum.SUCCESS.code);
        result.setMsg(ResultCodeEnum.SUCCESS.msg);
        result.setData(data);
        return result;
    }


    public static <T>  Result result(String code,String msg,T data){
        Result<T> result=new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        result.setData(data);
        return result;
    }



    public static <T>  Result result(String code,String msg){
        Result<T> result=new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }




}
