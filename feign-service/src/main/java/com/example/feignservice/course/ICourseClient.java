package com.example.feignservice.course;


import com.example.feignservice.common.Result;
import com.example.feignservice.course.fallback.CourseClientFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author closer
 */
@FeignClient(value = "course",fallbackFactory = CourseClientFallbackFactory.class)
public interface ICourseClient {

    /**
     * 获取学生所选课程信息
     * @param studentId
     * @return
     */
    @GetMapping(value = "/api/v1/course",params = {"studentId"})
    public Result getCourseByStudentId(@RequestParam("studentId") Long studentId);

}
