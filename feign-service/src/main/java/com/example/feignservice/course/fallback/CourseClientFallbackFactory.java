package com.example.feignservice.course.fallback;

import com.example.feignservice.common.Result;
import com.example.feignservice.course.ICourseClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * @author closer
 */
@Slf4j
public class CourseClientFallbackFactory implements FallbackFactory<ICourseClient> {

    @Override
    public ICourseClient create(Throwable throwable) {
        return new ICourseClient() {
            @Override
            public Result getCourseByStudentId(Long id) {
                log.error("查询课程失败！", throwable);
                return Result.result("500","调用方法失败");
            }
        };
    }
}
